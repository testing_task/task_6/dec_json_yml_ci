import yaml
import json
import os
import urllib.request
from pathlib import *
from urllib.error import HTTPError

url_address = os.environ['URL']
file_name = Path.cwd()/'file'

try:

    resource = urllib.request.urlopen(url_address)
    content =  resource.read().decode(resource.headers.get_content_charset())

    Path(file_name).write_text(content)
    print(yaml.dump(json.load(open(Path.cwd()/'file'))))

except HTTPError as ex:
    print(yaml.dump(json.loads(ex.read())))
